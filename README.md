# Clever Reach Rules
Minimalistic Clever Reach integration for rules.

This modules provides a [Clever Reach](https://www.cleverreach.com/) integration for 
drupal via [rules](https://www.drupal.org/project/rules).
There is currently one condition to check if a mail is already subscribed to the
a list, and four actions:
- to subscribe a mail to a list
- to subscribe a user to a list
- to unsubscribe a mail from a list
- to unsubscribe a mail from all lists list

The action to subscribe users also allows to sync merge fields and the language.
There is also an alter hook in that action, to add additional parameters.


## Getting started
- Enable the module
- Go to /admin/config/services/cleverreach-rules and set your API key and API WSDL URL
- Create a new rule and add one of the actions
