<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Alter the parameters of user that is about to be subscribed or updated.
 *
 * @param array $parameters
 *   An array of parameters, for example merge_fields, interest groups or
 *   language.
 * @param string $list
 *   The list id to which the user will be subscribed to.
 * @param object $account
 *   An user object containing the mail address which will subscribed.
 */
function hook_clever_reach_rules_user_subscribe_list_parameters_alter(array &$parameters, string $list, stdClass &$account) {
  // Change parameters.
}

/**
 * Alter the parameters of email that is about to be subscribed or updated.
 *
 * @param array $parameters
 *   An array of parameters, for example merge_fields, interest groups or
 *   language.
 * @param string $list
 *   The list id to which the user will be subscribed to.
 * @param string $mail
 *   A mail address which will be subscribed.
 */
function hook_clever_reach_rules_mail_subscribe_list_parameters_alter(array &$parameters, string $list, string $mail) {
  // Change parameters.
}
