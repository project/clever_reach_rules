<?php

/**
 * @file
 * Rules integration for the clever_reach_rules module.
 */

/**
 * Implements hook_rules_condition_info().
 */
function clever_reach_rules_rules_condition_info() {
  $items = array();

  $items['clever_reach_rules_mail_is_subscribed'] = array(
    'label' => t('Is a mail subscribed to list'),
    'parameter' => array(
      'list' => array(
        'type' => 'text',
        'label' => t('Clever Reach list'),
        'description' => t('Select an existing Clever Reach list from your account'),
        'options list' => 'clever_reach_rules_list_names',
      ),
      'mail' => array(
        'type' => 'text',
        'label' => t('Mail address'),
        'description' => t('The mail address to subscribe to the list'),
        'default mode' => 'selector',
      ),
    ),
    'group' => t('Clever Reach Rules'),
    'base' => 'clever_reach_rules_rules_condition_mail_is_subscribed',
  );

  return $items;
}

/**
 * Implements hook_rules_action_info().
 */
function clever_reach_rules_rules_action_info() {
  $items = array();

  $items['clever_reach_rules_mail_subscribe'] = array(
    'label' => t('Subscribe a mail to a Clever Reach list'),
    'parameter' => array(
      'list' => array(
        'type' => 'text',
        'label' => t('Clever Reach list'),
        'description' => t('Select an existing Clever Reach list from your account'),
        'options list' => 'clever_reach_rules_list_names',
      ),
      'mail' => array(
        'type' => 'text',
        'label' => t('Mail address'),
        'description' => t('The mail address to subscribe to the list'),
        'default mode' => 'selector',
      ),
      'merge_fields' => array(
        'type' => 'list<text>',
        'label' => t('Merge fields'),
        'description' => t("For each line you can enter a merge field in the format of 'merge_field': '[replacement_pattern]'. You can view the merge field tags in the settings for the selected list."),
        'restriction' => 'input',
        'optional' => TRUE,
      ),
      'language' => array(
        'type' => 'text',
        'label' => t('Language'),
        'description' => t("The language for the user, which should be subscribed."),
        'optional' => TRUE,
        'allow null' => TRUE,
        'default value' => language_default()->language,
      ),
    ),
    'group' => t('Clever Reach Rules'),
    'base' => 'clever_reach_rules_rules_action_mail_subscribe_list',
  );

  $items['clever_reach_rules_mail_unsubscribe'] = array(
    'label' => t('Unsubscribe a mail from a Clever Reach list'),
    'parameter' => array(
      'list' => array(
        'type' => 'text',
        'label' => t('Clever Reach list'),
        'description' => t('Select an existing Clever Reach list from your account'),
        'options list' => 'clever_reach_rules_list_names',
      ),
      'mail' => array(
        'type' => 'text',
        'label' => t('Mail address'),
        'description' => t('The mail address which should be unsubscribed.'),
        'default mode' => 'selector',
      ),
    ),
    'group' => t('Clever Reach Rules'),
    'base' => 'clever_reach_rules_rules_action_mail_unsubscribe_list',
  );

  $items['clever_reach_rules_mail_unsubscribe_all'] = array(
    'label' => t('Unsubscribe a mail from all Clever Reach lists'),
    'parameter' => array(
      'mail' => array(
        'type' => 'text',
        'label' => t('Mail address'),
        'description' => t('The mail address which should be unsubscribed.'),
        'default mode' => 'selector',
      ),
    ),
    'group' => t('Clever Reach Rules'),
    'base' => 'clever_reach_rules_rules_action_mail_unsubscribe_all_lists',
  );

  $items['clever_reach_rules_user_subscribe'] = array(
    'label' => t('Subscribe a user to a Clever Reach list'),
    'parameter' => array(
      'list' => array(
        'type' => 'text',
        'label' => t('Clever Reach list'),
        'description' => t('Select an existing Clever Reach list from your account'),
        'options list' => 'clever_reach_rules_list_names',
      ),
      'user' => array(
        'type' => 'user',
        'label' => t('User'),
        'description' => t('The user which should subscribe to the list'),
        'default mode' => 'selector',
      ),
      'merge_fields' => array(
        'type' => 'list<text>',
        'label' => t('Merge fields'),
        'description' => t("For each line you can enter a merge field in the format of 'merge_field': '[replacement_pattern]'. You can view the merge field tags in the settings for the selected list."),
        'restriction' => 'input',
        'optional' => TRUE,
      ),
      'language' => array(
        'type' => 'text',
        'label' => t('Language'),
        'description' => t("The language for the user, which should be subscribed."),
        'optional' => TRUE,
        'allow null' => TRUE,
        'default value' => language_default()->language,
      ),
    ),
    'optional' => TRUE,
    'group' => t('Clever Reach Rules'),
    'base' => 'clever_reach_rules_rules_action_user_subscribe_list',
  );

  return $items;
}

/**
 * Returns all the available Clever Reach lists for the rules.
 */
function clever_reach_rules_list_names(RulesPlugin $element, $param_name) {
  return clever_reach_rules_retrieve_lists();
}

/**
 * Callback for the mail is subscribed condition.
 */
function clever_reach_rules_rules_condition_mail_is_subscribed($list, $mail) {
  return clever_reach_rules_mail_is_subscribed($list, $mail);
}

/**
 * Callback to subscribe an email address to a Clever Reach list.
 */
function clever_reach_rules_rules_action_mail_subscribe_list($list, $mail, $merge_fields = array(), $language = NULL) {
  $parameters = clever_reach_subscribe_rules_build_parameters($merge_fields, $language);

  drupal_alter('clever_reach_rules_mail_subscribe_list_parameters', $parameters, $list, $mail);

  clever_reach_rules_subscribe_mail($list, $mail, $parameters);
}

/**
 * Callback to unsubscribe an email address from a Clever Reach list.
 */
function clever_reach_rules_rules_action_mail_unsubscribe_list($list, $mail) {
  clever_reach_rules_unsubscribe_mail($list, $mail);
}

/**
 * Callback to unsubscribe an email address from all Clever Reach lists.
 */
function clever_reach_rules_rules_action_mail_unsubscribe_all_lists($mail) {
  clever_reach_rules_unsubscribe_mail_all_lists($mail);
}

/**
 * Callback to subscribe an user to a Clever Reach list.
 */
function clever_reach_rules_rules_action_user_subscribe_list($list, $account, $merge_fields = array(), $language = NULL) {
  $parameters = clever_reach_subscribe_rules_build_parameters($merge_fields, $language);

  drupal_alter('clever_reach_rules_user_subscribe_list_parameters', $parameters, $list, $account);

  clever_reach_rules_subscribe_mail($list, $account->mail, $parameters);
}

/**
 * Builds parameters array for the clever reach request.
 *
 * @param array $merge_fields
 *   Array of merge fields.
 * @param string $language
 *   Language code.
 *
 * @return array
 *   Associative array with parameters.
 */
function clever_reach_subscribe_rules_build_parameters($merge_fields, $language) {
  $parameters = $fields = array();
  foreach ($merge_fields as $line) {
    if (!empty($line)) {
      $parts = array();
      // Fields should have to json like format 'key': 'value' where, the key is
      // the merge field tag/name and value is the value the should be used for
      // the merge key. Ignore any spaces around the apostrophes.
      preg_match("/^\s*'(.*)':\s*'(.*)'\s*$/", $line, $parts);

      // Save the merge field only if we have a key and a value.
      if (count($parts) == 3) {
        $fields[$parts[1]] = $parts[2];
      }
    }
  }

  if (!empty($fields)) {
    $parameters['merge_fields'] = $fields;
  }

  if (isset($language) &&  $language != LANGUAGE_NONE) {
    $parameters['language'] = substr($language, 0, 2);
  }
  return $parameters;
}
